﻿Imports SQLite

Public Class User

    <PrimaryKey, AutoIncrement>
    Public Property ID As Integer

    Public Property FirstName As String

    Public Property LastName As String

    Public Property DepartmentID As Integer

End Class
