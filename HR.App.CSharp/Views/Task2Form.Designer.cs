﻿namespace HR.App.CSharp.Views
{
    partial class Task2Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Task2Form));
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.Button3 = new System.Windows.Forms.Button();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.Label5 = new System.Windows.Forms.Label();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(152, 54);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Size = new System.Drawing.Size(73, 21);
            this.TextBox3.TabIndex = 31;
            this.TextBox3.Text = "Sarcone";
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(180, 348);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(75, 23);
            this.Button3.TabIndex = 30;
            this.Button3.Text = "Create";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(23, 306);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(463, 30);
            this.Label9.TabIndex = 29;
            this.Label9.Text = "Have this button create a \'User\' object and display the details of that user on s" +
    "creen.\r\nUse the name from Task 2.1\r\n";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Arial", 12F);
            this.Label10.Location = new System.Drawing.Point(23, 283);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(66, 18);
            this.Label10.TabIndex = 28;
            this.Label10.Text = "Task 2.3";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(23, 223);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(100, 15);
            this.Label8.TabIndex = 27;
            this.Label8.Text = "Times to display:";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(129, 220);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(45, 21);
            this.TextBox2.TabIndex = 26;
            this.TextBox2.Text = "5";
            // 
            // Label6
            // 
            this.Label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label6.Location = new System.Drawing.Point(23, 136);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(579, 77);
            this.Label6.TabIndex = 25;
            this.Label6.Text = "Finish the code for this section so that the name from 2.1 is displayed the same " +
    "number of times as the box below. \r\nPrefix the name with the index.\r\ne.g. 1. Jos" +
    "h Sarcone\r\n2.Josh Sarcone";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Arial", 12F);
            this.Label7.Location = new System.Drawing.Point(23, 111);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(66, 18);
            this.Label7.TabIndex = 24;
            this.Label7.Text = "Task 2.2";
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(180, 219);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(75, 23);
            this.Button2.TabIndex = 23;
            this.Button2.Text = "Display";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(231, 53);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(75, 23);
            this.Button1.TabIndex = 22;
            this.Button1.Text = "Greet !";
            this.Button1.UseVisualStyleBackColor = true;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(23, 57);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(44, 15);
            this.Label5.TabIndex = 21;
            this.Label5.Text = "Name:";
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(73, 54);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(73, 21);
            this.TextBox1.TabIndex = 20;
            this.TextBox1.Text = "Josh";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(23, 34);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(238, 15);
            this.Label3.TabIndex = 19;
            this.Label3.Text = "Fix the program so below name is greeted";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Arial", 12F);
            this.Label4.Location = new System.Drawing.Point(23, 11);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(66, 18);
            this.Label4.TabIndex = 18;
            this.Label4.Text = "Task 2.1";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(23, 464);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(377, 30);
            this.Label2.TabIndex = 17;
            this.Label2.Text = "Add/Include evidence of the testing done to complete all other tasks.\r\nTesting ca" +
    "n be carried out in any form you choose. ";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 12F);
            this.Label1.Location = new System.Drawing.Point(23, 441);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(53, 18);
            this.Label1.TabIndex = 16;
            this.Label1.Text = "Task 3";
            // 
            // Task2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(604, 540);
            this.Controls.Add(this.TextBox3);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.TextBox2);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Task2Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Task 2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}