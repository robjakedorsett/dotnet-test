﻿using HR.App.CSharp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HR.App.CSharp.Views
{
    public partial class Task2Form : Form
    {
        public Task2Form()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            string greeting = "Hello World!";
            MessageBox.Show(greeting, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            User person = new User();
            person = GetPerson();
            person.ToString();
        }

        public User GetPerson()
        {
            User person = null;
            return person;
        }

    }
}
