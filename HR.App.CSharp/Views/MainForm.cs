﻿using HR.App.CSharp.Entities;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HR.App.CSharp.Views
{
    public partial class MainForm : Form
    {
        private SQLiteConnection _database;
        public SQLiteConnection Database
        {
            get
            {
                if (_database == null)
                    _database = new SQLiteConnection(ConfigurationManager.AppSettings["path-to-database"]);
                return _database;
            }
        }

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var tbl = Database.Table<User>();

            foreach (User user in tbl)
            {
                ListViewItem userListitem = new ListViewItem(new string[] {
                    user.ID.ToString(),
                    user.FirstName,
                    user.LastName
                });
                UserListView.Items.Add(userListitem);
            }
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var task2 = new Task2Form();
            task2.ShowDialog();
        }
    }
}
